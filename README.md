# TEMPERplot

Little project to read sensors periodically and plot the data nicely into a graph.

Data acquisition should be made somewhat robust, though.

Device used for data acquisition is the [TEMPerX232 module from PC Sensor](https://www.pcsensor.com/usb-temperature-humidity/usb-hygrometers/temperx232.html).
