#! /usr/bin/env python3

import sys
import os
import time
import serial
import tzlocal
import datetime

def mkts():
    tz = tzlocal.get_localzone()
    tn = datetime.datetime.now(tz)
    return str(tn).split('.')[0]

def readTemp(device):
    S = serial.Serial(device, 9600)
    S.bytesize      = serial.EIGHTBITS
    S.parity        = serial.PARITY_NONE
    S.stopbits      = serial.STOPBITS_ONE
    S.timeout       = 1
    S.xonoff        = False
    S.rtscts        = False
    S.dsrdtr        = False
    S.writeTimeout  = 0

    temp = []
    sensordata = {}
    
    S.write(b"ReadTemp")

    l = str(S.readline(), "latin-1").strip()
    while l and not l == '\x00':
        temp.append(l.replace('<', ''))
        l = str(S.readline(), "latin-1").strip()

    S.close()
   
    for x in temp:
        sensor_block_id = x.split(':')[0].split('-')[1].lower()
        sensor_block_data_line = x.split(':')[1]
        sensor_block_data = list( map( lambda i: tuple(( float(i.split(' ')[0]), i.split(' ')[1].replace('[', '').replace(']', '') )), sensor_block_data_line.split(',') ) )
        sensordata.update({ sensor_block_id : sensor_block_data })

    for x in sensordata:
        data_dict = { "temperature" : None, "humidity" : None }

        for y in sensordata[x]:
            if y[1] == 'C' or y[1] == 'F':
                data_dict["temperature"] = { "value" : y[0], "unit" : y[1] }
            elif y[1] == '%RH':
                data_dict["humidity"] = { "value" : y[0], "unit" : y[1] }

        sensordata.update({ x : data_dict })

    return sensordata

def writeCsv(data, sensor):
    with open(f"{data['date']}_{sensor}_sec.csv", 'a+') as F:
        F.write(f"{data['ts']}," + str(data[sensor]) + '\n')
        F.close()

def main():
    import pprint
    #prev_val = { "temp": -100, "rh": -100 }
    prev_val = {}
    while True:
        DATA = {}
        try:
            DATA.update({ "sensors" : readTemp("/dev/ttyUSB0") })
        except KeyboardInterrupt:
            return 0
        DATA.update({ "ts" : { "date" : mkts().split(' ')[0], "time" : mkts().split(' ')[1] } })

        # DATA package fully formed at this point
        
        fringe = False
        
        if DATA['ts']['time'] == "00:00:00" or DATA['ts']['time'] == "23:59:59":
            fringe = True

        print("--- tick ---")

        if not prev_val:
            print(">>> FIRST MEASUREMENT")
            try:
                print(f"{k} temp: {v['temperature']['value']}°{v['temperature']['unit']}")
                print(f"{k} humidity: {v['humidity']['value']}{v['humidity']['unit']}")
            except TypeError:
                pass
                prev_val = DATA['sensors']

        for k, v in DATA['sensors'].items():
            try:
                if prev_val[k]['temperature'] != v['temperature'] or fringe:
                    #writeCsv(data, f"temp-{k}")
                    print(f"{k} temp: {v['temperature']['value']}°{v['temperature']['unit']}")
                    prev_val[k]['temperature'] = v['temperature']
                if prev_val[k]['humidity'] != v['humidity'] or fringe:
                    #writeCsv(data, f"rh-{k}")
                    print(f"{k} humidity: {v['humidity']['value']}{v['humidity']['unit']}")
                    prev_val[k]['humidity'] = v['humidity']

            except KeyError: # prev_val empty
                pass
        #sys.exit()

        #if prev_val['temp'] != data['temp'] or fringe:
        #    writeCsv(data, "temp")
        #    print(f"{data['ts']} {data['temp']:5.2f}°C")

        #if prev_val['rh'] != data['rh'] or fringe:
        #    writeCsv(data, "rh")
        #    print(f"{data['ts']} {data['rh']:5.2f}%")

        #prev_val = data

        try:
            time.sleep(1)
        except KeyboardInterrupt:
            return 0


if __name__ == "__main__":
    main()
