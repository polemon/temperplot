#! /usr/bin/env python3

import datetime as dt
from datetime import datetime as dtdt
import csv
import sys

fn_date = "2020-08-18"
fn_type = "cpu"

def csvPrint(F, data):
    F.write(','.join( map( lambda x: f"{x}" if isinstance(x, str) else f"{float(x):f}", data ) ) + '\n')

def main():
    try:
        sourceF = sys.argv[1]   # source file: 2020-08-18_temp_min.csv
    except:
        print("no source file given!")
        return 1

    try:
        destinationF = sys.argv[2]   # optional out-file
    except:
        destinationF = sourceF.replace("min", "hour")

        if destinationF == sourceF:
            destinationF = f"{sourceF.split('.')[0]}_hour.{sourceF.split('.')[1]}"

    with open(sourceF, 'r') as F:
        acc = []
        prev_row = []
        prev_t = None
        lastval = None

        with open(destinationF, 'w+') as C:
            for row in csv.reader(F, delimiter = ','):
                curr_t = dt.time.fromisoformat(row[0])

                try:
                    prev_t = dt.time.fromisoformat(prev_row[0])
                except IndexError: # handle first line in input file
                    acc.clear()
                    acc.append([ curr_t.minute, float(row[1]) ])
                    prev_row = row
                    continue

                if (prev_t.hour == curr_t.hour): # add hour fractions
                    acc.append([ curr_t.minute - prev_t.minute, float(prev_row[1]) ])

                else:   # next hour
                    acc.append([ 60 - prev_t.minute, float(prev_row[1]) ]) # finish previous hour
                    avg = sum(map(lambda t: t[0] * t[1], acc)) / 60
                    if (avg != lastval):
                        csvPrint(C, [ prev_t.isoformat(timespec = 'hours'), avg ])
                        lastval = avg

                    # handle more than one minute between values
                    if ( (dtdt.combine(dtdt.today(), curr_t) - dtdt.combine(dtdt.today(), prev_t)).seconds > 7140 
                         and (lastval != float(prev_row[1])) ):
                        nextmin = (dtdt.combine(dtdt.today(), prev_t) + dt.timedelta(hours = 1)).time()
                        csvPrint(C, [ nextmin.isoformat(timespec = 'hours'), float(prev_row[1]) ])
                        lastval = float(prev_row[1])

                    # first part of next minute
                    acc.clear()
                    acc.append([ curr_t.minute, float(prev_row[1]) ])

                prev_row = row

            ## handling last row in input file
            try:
                prev_t = dt.time.fromisoformat(prev_row[0])
                acc.append([ 60 - prev_t.minute, float(prev_row[1]) ])
                avg = sum(map(lambda t: t[0] * t[1], acc)) / 60
                csvPrint(C, [ prev_t.isoformat(timespec = 'hours'), avg ])
            except IndexError:
                # source file empty
                return 1

        C.close()
    F.close()

if __name__ == "__main__":
    main()
