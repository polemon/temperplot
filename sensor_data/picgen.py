#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.dates as mdates
import matplotlib.font_manager as fman
import math
import csv
import sys

g_sel = "min"
t_sel = "2020-08-20"
fontfile = "/home/bereziak/.fonts/Futura/FuturaStd-CondensedLight.otf"

def readScatter(granulity, g_sel, filename):
    y = [ -100 ] * granulity

    with open(filename, 'r+') as F:
        prior_ts = None

        for row in csv.reader(F, delimiter = ','):
            if not prior_ts and not prior_ts == 0:
                if g_sel == "sec": 
                    prior_ts = (int(row[0].split(':')[0]) * 60 * 60) + (int(row[0].split(':')[1]) * 60) + int(row[0].split(':')[2])
                elif g_sel == "min":
                    prior_ts = (int(row[0].split(':')[0]) * 60) + int(row[0].split(':')[1])
                elif g_sel == "hour":
                    prior_ts = int(row[0])

                y[prior_ts] = float(row[1])
                continue

            if g_sel == "sec": 
                curr_ts = (int(row[0].split(':')[0]) * 60 * 60) + (int(row[0].split(':')[1]) * 60) + int(row[0].split(':')[2])
            elif g_sel == "min":
                curr_ts = (int(row[0].split(':')[0]) * 60) + int(row[0].split(':')[1])
            elif g_sel == "hour":
                curr_ts = int(row[0])
            
            y[curr_ts] = float(row[1])

            for i in range(prior_ts, curr_ts):
                y[i] = y[prior_ts]

            prior_ts = curr_ts
        F.close()

    return y

def main():
    g_sel = ""
    t_sel = ""

    try:
        g_sel = sys.argv[2]
        if g_sel == "sec":
            granulity = 24 * 60 * 60
        elif g_sel == "min":
            granulity = 24 * 60
        elif g_sel == "hour":
            granulity = 24
        else:
            raise

        t_sel = sys.argv[1]
    except:
        print("no!")
        return 1

    prop = fman.FontProperties(fname = fontfile)

    fig, ax = plt.subplots(figsize = (25, 10))
    fig.set_dpi(300)
    
    font = { "fontweight":"normal",
             "fontsize":"x-large" }

    ax.set_axisbelow(True)
    ax.xaxis.grid(True, which = "minor", ls = '-', color = "#f0f0f0", linewidth = 0.5)
    ax.xaxis.grid(True, which = "major", ls = '-', color = "#707070", linewidth = 0.5)
    ax.yaxis.grid(True, which = "both",  ls = '-', color = "#c0c0c0", linewidth = 0.5)
    ax.xaxis.set_tick_params(which = "major", width = 0.4, length = 8)
    ax.xaxis.set_tick_params(which = "minor", width = 0.4, length = 2)
    ax.yaxis.set_tick_params(width = 0.4, length = 8)

    ax.set_ylabel("°C", rotation = 0, labelpad = 10, **font, fontproperties = prop)
    ax.set_ylim(0, 110)
    ax.set_yticks(np.arange(0, 110 +1, 5))
    ax.set_yticklabels(np.arange(0, 110 +1, 5), **font, fontproperties = prop)
    ax.set_xlabel("time", rotation = 0, labelpad = 10, **font, fontproperties = prop)
    ax.set_xticks(np.arange(0, granulity +1, 60))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
    ax.set_xticklabels(map(lambda x: f"{x // 60:2d}:{x % 60:02d}", np.arange(0, granulity+1, 60)), rotation = 0, **font, fontproperties = prop)

    for axis in [ 'top', 'bottom', 'left', 'right' ]:
        ax.spines[axis].set_linewidth(0.4)

    x = range(0, granulity)

    y = readScatter(granulity, g_sel, f"{t_sel}_temp_mb_{g_sel}.csv")
    #ax.scatter(x, y, s = 0.1, c = "darkgreen", marker = 's', alpha = 0.5, label = "MB")
    ax.plot(x, y, color = "darkgreen", marker = '.', markersize = 0.5, linewidth = 0.1, alpha = 0.5, label = "MB")
    
    y = readScatter(granulity, g_sel, f"{t_sel}_temp_gpu_{g_sel}.csv")
    #ax.scatter(x, y, s = 0.1, c = "darkblue", marker = 's', alpha = 0.5, label = "GPU")
    ax.plot(x, y, color = "darkblue",  marker = '.', markersize = 0.5, linewidth = 0.1, alpha = 0.5, label = "GPU")
    
    y = readScatter(granulity, g_sel, f"{t_sel}_temp_cpu_{g_sel}.csv")
    #ax.scatter(x, y, s = 0.1, c = "darkred", marker = 's', alpha = 0.5, label = "CPU")
    ax.plot(x, y, color = "darkred",   marker = '.', markersize = 0.5, linewidth = 0.1, alpha = 0.5, label = "CPU")
    
    legends = ax.legend(loc = "upper left", markerscale = 10)
    for l in legends.get_lines():
        l.set_linewidth(2.0)

    plt.xlim([0, granulity])
    plt.title(f"{t_sel} temperature graph [granulity: {g_sel}]", pad = 30, **font, fontproperties = prop)

    plt.subplots_adjust(left = 0.05, right = 0.95, top = 0.90, bottom = 0.10)
    plt.savefig(f"{t_sel}_temp_{g_sel}.pdf", transparent = False, dpi = 300)

if __name__ == "__main__":
    main()
