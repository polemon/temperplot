#! /usr/bin/env python3

import subprocess as sp
import json
import datetime
import tzlocal
import time

def gettemp():
    res = sp.run(['sensors', '-j'], stdout = sp.PIPE).stdout.decode('utf-8')
    js = json.loads(res)
   
    t_cpu = float('inf')
    t_mb = float('inf')
    t_gpu = float('inf')

    for i in js:
        for j in js[i]['CPU Temperature']:
            if "input" in j:
                t_cpu = float(js[i]['CPU Temperature'][j])

        for j in js[i]['MB Temperature']:
            if "input" in j:
                t_mb = float(js[i]['MB Temperature'][j])

    res = sp.run(['nvidia-smi', '-q', '-d', 'temperature'], stdout = sp.PIPE).stdout.decode('utf-8').splitlines()
    
    for l in res:
        if "GPU Current" in l:
            t_gpu = float(l.split(':')[1].split('C')[0])

    return [ t_cpu, t_mb, t_gpu ]

def mkts():
    tz = tzlocal.get_localzone()
    tn = datetime.datetime.now(tz)
    return str(tn).split('.')[0]

def main():
    previous = [ float('inf'), float('inf'), float('inf') ]

    while True:
        temps = gettemp()
        isots = mkts()

        date = isots.split(' ')[0]
        ts = isots.split(' ')[1]
        
        fringe = False

        if ts == "00:00:00" or ts == "23:59:59":
            fringe = True

        if previous[0] != temps[0] or fringe:
            Fcpu = open(f"{date}_temp_cpu_sec.csv", 'a+')
            Fcpu.write(f"{ts},{temps[0]}\n")
            Fcpu.close()
            print(f"CPU: {ts}, {temps[0]}")
            previous[0] = temps[0]
        
        if previous[1] != temps[1] or fringe:
            Fmb = open(f"{date}_temp_mb_sec.csv", 'a+')
            Fmb.write(f"{ts},{temps[1]}\n")
            Fmb.close()
            print(f"MB:  {ts}, {temps[1]}")
            previous[1] = temps[1]

        if previous[2] != temps[2] or fringe:
            Fgpu = open(f"{date}_temp_gpu_sec.csv", 'a+')
            Fgpu.write(f"{ts},{temps[2]}\n")
            Fgpu.close()
            print(f"GPU: {ts}, {temps[2]}")
            previous[2] = temps[2]

        try:
            time.sleep(1)
        except KeyboardInterrupt:
            return 0

if __name__ == "__main__":
    main()
